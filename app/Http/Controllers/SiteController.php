<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CustomerFeedback;
use App\CustomerFeedbackDetail;
use App\Customer;
use App\Tables;
use App\Question;
use Auth;
use Storage;
use Hash;
use DataTables;
use Validator;

class SiteController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function login()
    {
        return view('web.Login');
    }

    public function logout(Request $request){
        Auth::guard('admin')->logout();
        return back();
    }

    public function loginWaiter(Request $request){
        $validator = Validator::make($request->all(),[
            "email"     => 'required|email',
            "password"  => "required"
        ]);
        if($validator->fails()){
            return back()->with('msg','Invalid Credentials');
        }
        $userdata = array(
            'password'  => $request->password,
            'email'     => $request->email
        );
        if (Auth::guard('waiter')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            Auth::guard('waiter')->login(User::where('email', $request->email)->first()); // It's only working for id=1 and it only works if I add Auth::login manually
            return redirect()->route('select_table');
        }
        return back()->with('msg','Invalid Credentials');
    }

    public function selectTable()
    {
        $data = [
            'tables'    =>  Tables::get(),
        ];
        return view('web.select_table',$data);
    }

    public function foodTaste(Request $request)
    {
        if($request->table)
        {
            \Cookie::queue('table_id', $request->table, 3600);
            // $table_id = \Cookie::get('table_id');   
        }
        $data = [
            'question'  =>  Question::where('question','LIKE','%taste%')->first(),
        ];
        return view('web.food_taste',$data);
    }

    public function foodQuality(Request $request)
    {
        $review = [
            'question'  => $request->question, 
            'option'    => $request->option,
        ];
        // return $review;
        $reviews = json_encode($review);
        \Cookie::queue('review1', $reviews, 3600);
        $data = [
            'question'  =>  Question::where('question','LIKE','%quality%')->first(),
        ];
        return view('web.Food_Quality',$data);
    }

    public function ambiance(Request $request)
    {
        $review = [
            'question'  => $request->question, 
            'option'    => $request->option,
        ];
        $reviews = json_encode($review);
        \Cookie::queue('review2', $reviews, 3600);
        $data = [
            'question'  =>  Question::where('question','LIKE','%ambiance%')->first(),
        ];
        return view('web.ambiance',$data);
    }

    public function service(Request $request)
    {
        $review = [
            'question'  => $request->question, 
            'option'    => $request->option,
        ];
        $reviews = json_encode($review);
        \Cookie::queue('review3', $reviews, 3600);
        $data = [
            'question'  =>  Question::where('question','LIKE','%service%')->first(),
        ];

        return view('web.Service',$data);
    }

    public function details(Request $request)
    {
        $review = [
            'question'  => $request->question, 
            'option'    => $request->option,
        ];
        $reviews = json_encode($review);
        \Cookie::queue('review4', $reviews, 3600);

        return view('web.add_your_details');
    }

    public function thankyou(Request $request)
    {
        $detail = [
            'full_name'     => $request->name, 
            'phone'         => $request->phone,
            'email'         => $request->email,
        ];
        $customer = Customer::create($detail);
        $comment = [
            'customer_id'       => $customer->id, 
            'table_number'      => \Cookie::get('table_id'),
            'comments'          => $request->comments,
        ];
        $feedback = CustomerFeedback::create($comment);

        for ($i=1; $i <5 ; $i++) { 
            $review[$i] =json_decode(\Cookie::get('review'.$i));
            \Cookie::forget('review'.$i);
        }
        $rev = [];
        foreach ($review as $key => $value) {
       
            $rev['question_id'] = $value->question;
            $rev['option_id'] = $value->option;
            $rev['feedback_id'] = $feedback->id;
            CustomerFeedbackDetail::create($rev);   
        }
        \Cookie::forget('table_id');
        return view('web.thank_u');
    }

}
