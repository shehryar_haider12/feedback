<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Question;
use Storage;
use DataTables;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('question.question');
    }

    public function datatable()
    {
        $questions = Question::all();
        return DataTables::of($questions)->make();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('question.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'question'          =>  'required|unique:question,question',
            'sort_order'        =>  'required|numeric',
            'question_type'     =>  'required',
        ]);
        Question::create($data);
        return redirect()->route('table');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        return "dsfadf";
        $data=[
            'isEdit' => true,
            'question' => $question
          ];
          return view('question.add',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $data = $request->validate([
            'question'          =>  'required|unique:question,question,'.$question,
            'sort_order'        =>  'required|numeric',
            'question_type'     =>  'required',
        ]);
        $question->update($data);
        return redirect()->route('question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $services = Question::findOrFail($request->id);
        // apply your conditional check here
        if ( $services->delete()) {
            $response['success'] = 'This Table Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
