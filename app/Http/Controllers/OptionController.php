<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Option;
use Storage;
use DataTables;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('options.options');
    }

    public function datatable()
    {
        $options = Option::all();
        return DataTables::of($options)->make();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('options.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'option'        =>  'required|unique:options,option',
            'sort_order'    =>  'required|numeric',
            'icon'          =>  'required',
        ]);
        $data['icon'] = Storage::disk('icons')->putFile('',$request->icon);
        Option::create($data);
        return redirect()->route('table');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Option $option)
    {
        $data=[
            'isEdit' => true,
            'option' => $option
          ];
          return view('options.add',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Option $option)
    {
        $data = $request->validate([
            'option'          =>  'required|unique:options,option,'.$option->id,
            'sort_order'        =>  'required|numeric',
            'icon'              =>  'nullable',
        ]);
        if($request->icon)
        {
            Storage::disk('icons')->delete($option->icon);
            $data['icon'] = Storage::disk('icons')->putFile('',$request->icon);
        }
        $option->update($data);
        return redirect()->route('option');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $services = Option::findOrFail($request->id);
        // apply your conditional check here
        if ( $services->delete()) {
            $response['success'] = 'This Table Deleted Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
