<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CustomerFeedback;
use Storage;
use DataTables;

class FeedBackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('feedback.feedback');
    }

    public function datatable()
    {
        $feedback = CustomerFeedback::with('customer','feedbackDetails')->orderBy('created_at','desc')->get();
        return DataTables::of($feedback)->make();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view(CustomerFeedback $feedback)
    {
        $feedback->update(['is_readed'=>1]);
        // return $feedback;
        $data=[
            'feedback' => $feedback
          ];
          return view('feedback.detail',$data);
    }
}
