<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CustomerFeedback;
use App\Customer;
use Auth;
use Storage;
use Hash;
use DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        // return transaction();
        $data = [
            
            'total_feedback'    =>  CustomerFeedback::count(),
            'today_feedback'    =>  CustomerFeedback::whereDate('created_at',now())->count(),
            'users'             =>  Customer::count(),
            'unique_visitor'    =>  Customer::where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-7 days')) )->count(),
            'today_users'       =>  Customer::whereDate('created_at',now())->take(6),
        ];

        return view('dashboard',$data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function customers()
    {
        return view('customers.customers');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function customerDatatable()
    {
        $customers = WebUser::whereNull('deleted_at')->where('member',0)->select(['id','avatar','full_name','dob','address','email','licence','verified','created_at']);
        return DataTables::of($customers)->make();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        $data = [
            'user' => Auth::user(),
        ];

        return view('settings', $data);
    }

    public function patch(Request $request)
    {
        $request->validate([
            'full_name'        => 'required|string|max:255',
            'email'             => "required|max:191|unique:users,email,".Auth::user()->id,
            'current_password'  => 'required_if:change_password,1',
            'password'          => 'required_if:change_password,1|confirmed|min:6|max:22',
            'avatar'            => 'nullable|image|max:3000',
        ]);

        if($request->change_password == 1 && !Hash::check($request->current_password, Auth::user()->password))
        {
            return redirect()->back()->withErrors(['current_password'=>'your current password does not match'])->withInput($request->all());

        }

        $input_data = $request->except('password', 'password_confirmation','_token','_method','change_password','current_password');

        if($request->password)
        {
            $input_data['password'] = bcrypt($request->input('password'));
        }

        if($request->avatar)
        {
            Storage::disk('uploads')->delete(Auth::user()->avatar);
            $input_data['avatar'] = Storage::disk('uploads')->putFile('', $request->avatar);
        }

        Auth::user()->update($input_data);

        return redirect()->route('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bannerDelete($banner)
    {
        $banner = Banner::findOrFail($banner);
        // return $banner;
        Storage::disk('uploads')->delete($banner->banner);
        // apply your conditional check here
        $banner->delete();
        return redirect()->back();
    }
}
