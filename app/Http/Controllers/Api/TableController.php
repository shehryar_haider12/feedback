<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tables;
use App\Question;
use App\Option;

class TableController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;
    public function tables()
    {
        $table=Tables::all();
        return $table;
    }
	
    public function questions()
    {
        $all=Question::
        select(['id','question','question_type'])
        ->orderBy('sort_order','asc')
        ->get();
        return result($all, $this->successStatus, 'All Questions');
    }

    public function options()
    {
        $all=Option::
        select(['id','option','icon'])
        ->orderBy('sort_order','asc')
        ->get();
        return result($all, $this->successStatus, 'All Options');
    }
}
