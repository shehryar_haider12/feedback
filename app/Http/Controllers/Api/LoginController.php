<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use Hash;
use Storage;

class LoginController extends Controller
{

    public $successStatus = 200;
    public $errorStatus = 401;

    public function users()
    {
        $user=User::all();
        return $user;
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'email'     =>  'required|max:11|exists:users,email',
            'password'  =>  'required',
        ]);

        $user_data = array(
            'email'  => $request->email,
            'password' => $request->password
        );
        $user = User::where('email',$request->email)
        ->where('role','employ')
        ->first();
        if($user == null)
        {
            return result('invalid email or password',$this->errorStatus, "error");
        }
        else
        {
            
            if(Hash::check($request->password, $user->password))
            {

                User::where('id',$user->id)
                ->update([
                    'is_active' => 1
                ]);
                $device_token = rand();
                User::where('id',$user->id)
                ->update(['device_token'=>$device_token]);
		$success['token'] =  $user->createToken('MyApp')->accessToken;
		$success['user'] = $user;
                return result($success,$this->successStatus, "success");
            }
            else{
                return result('invalid email or password',$this->errorStatus, "error");

            }
        }
    }
}
