<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use App\CustomerFeedback;
use App\CustomerFeedbackDetail;
use App\Question;
use Auth;

class CustomerFeedbackController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

    public function feedback(Request $request)
    {
        $request->validate([
            'full_name'     => 'required',
            'email'         => 'nullable',
            'phone'         => 'required|max:11',
            'user_id'       => 'required',
            'option_id'     => 'required|array',
            'question_id'   => 'required|array',
        ]);
        $customer = Customer::where('phone',$request->phone)->first();
        if(!empty($customer))
        {
            $c = $customer;    
        }
        else{
            $c = Customer::create([
                'full_name' => $request->full_name,
                'email'     => $request->email,
                'phone'     => $request->phone,
                'user_id'   => $request->user_id
            ]);
        }
        $feedback = CustomerFeedback::create([
            'comments'      => $request->comments,
            'customer_id'   => $c->id,
            'table_number'  => $request->table_number
        ]);
        
        foreach ($request->question_id as $key => $question) {
            $data = [
                'feedback_id'   => $feedback->id,
                'question_id'   => $question,
                'option_id'     => $request->option_id[$key],
            ];
            CustomerFeedbackDetail::create($data);
        }
        return result($request->all(),$this->successStatus, "success");
    }
}
