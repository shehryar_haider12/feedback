<?php
use App\Option;

 function result($data, $status, $message)
 {
    return response()->json([
        "data"      =>  $data,
        "code"      =>  $status,
        "message"   =>  $message,
        "image_url" =>  url('')."/uploads/",
    ], $status);
 }

 function options()
 {
     return Option::where('status',1)->get();
 }

/**
 * Get Specified Site Setting
 * @return array 
 */
//  function getSiteSetting($key)
//  {
//     $setting =  Setting::where('key',$key)->first();
//     return $setting->value;
//  }

//  /**
//  * Get Specified Site Setting
//  * @return array 
//  */
// function loginHistory($data)
// {
//    $data['datetime'] = now();
//    LoginHistory::create($data);  
// }