<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;
use App\Option;

class CustomerFeedbackDetail extends Model
{
    protected $table = 'customer_feedback_detail';
    protected $fillable = [
        'question_id',
        'option_id',
        'feedback_id',
    ];

    /**
     * Get the question associated with the CustomerFeedbackDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function question()
    {
        return $this->hasOne(Question::class, 'id', 'question_id');
    }

    /**
     * Get the question associated with the CustomerFeedbackDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function option()
    {
        return $this->hasOne(Option::class, 'id', 'option_id');
    }
}
