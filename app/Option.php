<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Option extends Model
{
    use SoftDeletes;
    
    protected $table = 'options';
    protected $fillable = [
        'option',
        'icon',
        'sort_order',
        'status',
    ];
}
