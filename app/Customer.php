<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customer';
    protected $primaryKey = 'id';
    protected $fillable = [
        'full_name',
        'email',
        'phone',
        'is_active',
        'is_deleted',
        'user_id',
    ];
}
