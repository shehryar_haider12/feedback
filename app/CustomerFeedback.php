<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;
use App\CustomerFeedbackDetail;

class CustomerFeedback extends Model
{
    protected $table = 'customer_feedback';
    protected $primaryKey = 'id';
    protected $fillable = [
        'comments',
        'customer_id',
        'table_number',
        'is_readed',
        'status',
    ];

    /**
     * Get the customer associated with the CustomerFeedback
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }

    /**
     * Get all of the feedbackDetails for the CustomerFeedback
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedbackDetails()
    {
        return $this->hasMany(CustomerFeedbackDetail::class, 'feedback_id', 'id');
    }
}
