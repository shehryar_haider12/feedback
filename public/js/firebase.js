
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBvS9cptoOaNOujTs2wbYVT3MQLx9Ki55Q",
    authDomain: "zerk-50530.firebaseapp.com",
    databaseURL: "https://zerk-50530.firebaseio.com",
    projectId: "zerk-50530",
    storageBucket: "zerk-50530.appspot.com",
    messagingSenderId: "25192219527",
    appId: "1:25192219527:web:220dfd5033cde115af0922",
    measurementId: "G-8EHL9FFS7D"
  };

  firebase.initializeApp(firebaseConfig);

//   messaging ( app ? :  App ) : Messaging;
  const messaging = firebase.messaging();
        messaging
        
        .requestPermission()
        .then(function(){
        
            console.log("Notification Permission Granted!");
            return messaging.getToken();
        
        }).then(function(token){
            $('#device_token').val(token);
            console.log(token);

        }).catch(function(err){
            console.log("Unable to get permission to notify.",err);
        
        });
  
        // Message on Payload
messaging.onMessage((payload) => {
    var user_detail = JSON.parse(payload.notification.title);
    var notification_count = $('.number-alert').html();

    $('.data-notify').append(`
        <a href="" class="dropdown-item" style="white-space:inherit">
            <i class="fas fa-user mr-2"></i><span class="new-message">new notification from </span><span class="from-person" style="color: #38afc2; font-size: 14px;"> Zerk </span> <span class="admin" style="font-size: 12px"> (Admin) </span> <span class="float-right text-muted text-sm notify-date">`+moment().format('DD-MM-YYYY HH:mm:s')+`</span>
        </a>
        <div class="dropdown-divider"></div>
    `);
    $('.unread_mesg').empty().html(Number(notification_count) + 1 + ' unread notifications');
    // $('.from-person').empty().html(user_detail.full_name +' '+ user_detail.last_name);
    // $('.notify-date').empty().html(moment().format('DD-MM-YYYY HH:mm:s'))
    // $('.admin').empty().html('(admin)');
    // $('.new-message').empty().html('new notification from');
    $('.number-alert').empty().html(Number(notification_count) + 1);

    console.log(payload);
}, e => {
    console.log(e)
  })