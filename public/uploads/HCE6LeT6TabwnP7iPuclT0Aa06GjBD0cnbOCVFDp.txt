,,,,
,,,,
,,,,
,,name,Unit,Cost
,,3-Ply Surgical Face Mask With Nose Pin,1,531
,,3-Ply Surgical Face Mask Without Nose Pin,1,632
,,Carbomer 940 20000gm,1,
,,Disinfectant 30000ml,1,3900
,,Disinfecting Spray 500ml,1,195
,,Dispenser 300 ml,1,927
,,Disposable Gown,1,300
,,Face Shield Colorful,1,142
,,Face Shield Large,1,142
,,Face Shield Small,1,142
,,Hand Sanitizer 1000ml,1,740
,,Hand Sanitizer 100ml,1,100
,,Hand Sanitizer 5000ml,1,3150
,,Hand Sanitizer 60ml,1,63
,,Hand Sanitizer 70ml,1,70
,,Hand Sanitizer Fliptop 250ml,1,208
,,Hand Sanitizer Fliptop 500ml,1,362
,,Hand Sanitizer Liquid 30000ml,1,19500
,,Hand Sanitizer Pump 150ml,1,175
,,Hand Sanitizer Pump 250ml,1,232
,,Hand Sanitizer Pump 500ml,1,405
,,Handwash (Anti-Bacterial) 500ml,1,168
,,Handwash (Anti-Bacterial) Blue 5000ml,1,800
,,Handwash (Anti-Bacterial) Fliptop 250ml,1,90
,,Handwash (Anti-Bacterial) Pump 250ml,1,115
,,Handwash (Anti-Bacterial) white 30000ml,1,4800
,,Handwash 30000ml,1,2250
,,Handwash 5000ml,1,475
,,Handwash pump 250ml,1,94
,,Handwash pump 500ml,1,122
,,Infrared Thermometer,1,6100
,,KN95 With Filter,1,375
,,KN95 Without Filter,1,375
,,Latex Gloves Large,1,928
,,Latex Gloves Medium,1,928
,,N95 8210,1,1585
,,N95 8210v,1,1722
,,Oximeter,1,2700
,,PPE Suit,1,515
,,Super Touch Polysynthetic Gloves,1,843
,,Super Touch Polysynthetic Gloves Large (Blue),1,843
,,Z. Hand Sanitizer 10000ml,1,5500
,,Z. Hand Sanitizer 100ml,1,88
,,Z. Hand Sanitizer 5000ml,1,2750
,,Z. Hand Sanitizer 60ml,1,69
,,Z. Hand Sanitizer Fliptop 280ml,1,170
,,Z. Hand Sanitizer Fliptop 500ml,1,320
,,Z. Hand Sanitizer Pump 280ml,1,204
,,Z. Hand Sanitizer Pump 500ml,1,360
,,Z. Hand Sanitizing Spray 120 ml,1,100
,,Z. Kids Sanitizer 60ml Purple,1,40
,,Z. Kids Sanitizer 60ml Blue,1,40
,,Kids Mask ,1, 180 
,,Polyster mask purchased,1,90.06
,,Hyronic Acid Serum 30ml,1,650
,,Vitamin C Face Serum,1,650
,,Aloe vera gel 500 ml,1,650
,,Petroleum Jelly 25 KG Bottle,1,200
,,JAR 100G Bottle,1,120
,,Shea Butter Spain,1,2000
,,Lotion Pump Bottle 100ml,1,240
,,Butter JAR 300G,1,145
,,Hyaluronic acid 30ml Sticker,1,18
,,Hyaluronic Acid Serum Stickers,1,6
,,Alovera Gell Boxes ,1,22
,,Alovera Gell 100ml Stickers,1,10
,,Alovera Gell 500ml Stickers,1,15
,,Vitam C Serum Boxes 30ml ,1,18
,,Vitam C Serum Stickers ,1,5.5
,,Petroluem Jelly jar 50ml ,1,13
,,Petroluem Jelly jar 100ml ,1,17
,,Pet Bottles 500ml 45gm ,1,15
,,28mm Bottle Pump ,1,32
,,Disinfectant Material,2,130
,,Hand Sanitizer Liquid Material,2,650
,,Handwash (Anti-Bacterial) Blue Material,2,160
,,Handwash (Anti-Bacterial) white,2,105
,,Handwash White Material,2,85
,,Z. Hand Sanitizer Material,2,550
