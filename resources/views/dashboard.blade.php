@extends('layouts.master')

@section('title', 'Dashboard')

@section('top-styles')
<style>
    .inner span{
        font-size: 12px;
    }
</style>
@endsection

@section('content')
@section('breadcrumb','Dashboard')

<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{$total_feedback ?? 0}}</h3>

                    <h5>Total Feedback</h5>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{$today_feedback ?? 0}}</h3>
                    <h5>Today Feedback</h5>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{$users ?? 0}}</h3>

                    <h5>User Registration</h5>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{$unique_visitor}}</h3>

                    <h5>Unique Visitors</h5>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable" style="display: none">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
             
                <div class="card-body">
                    <div class="tab-content p-0">
                        <!-- Morris chart - Sales -->
                            <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>
                            <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>
                    </div>
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- /.card -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-12 connectedSortable" style="margin: 0 auto;">
            <div class="row">
            <!-- Map card -->
            <div class="card bg-gradient-primary" style="display: none">
                
                <div class="card-body">
                    {{-- <div id="world-map" style="height: 250px; width: 100%;"></div> --}}
                </div>
                <!-- /.card-body-->
                <div class="card-footer bg-transparent">
                    <div class="row">
                        <div class="col-4 text-center">
                            <div id="sparkline-1"></div>
                            {{-- <div class="text-white">Visitors</div> --}}
                        </div>
                        <!-- ./col -->
                        <div class="col-4 text-center">
                            <div id="sparkline-2"></div>
                            {{-- <div class="text-white">Online</div> --}}
                        </div>
                        <!-- ./col -->
                        <div class="col-4 text-center">
                            <div id="sparkline-3"></div>
                            {{-- <div class="text-white">Sales</div> --}}
                        </div>
                        <!-- ./col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.card -->

            <!-- solid sales graph -->
            <div class="col-md-6">
                <div class="card bg-gradient-info" >
                    <div class="card-header border-0">
                        <h3 class="card-title">
                            <i class="fas fa-th mr-1"></i>
                            Today Users Registration
                        </h3>
                        <br>
                        <hr>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td>
                                        S.No
                                    </td>
                                    <td>
                                        Full Name
                                    </td>
                                    <td>
                                        Phone
                                    </td>
                                </tr>

                            </thead>
                            <tbody>
                                @php
                                    $i = 0;
                                @endphp
                                @if ($today_users->count() > 0)
                                    
                                @foreach ($today_users as $user)
                                
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$user->full_name}}</td>
                                    <td>{{$user->phone}}</td>
                                </tr>
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">No Data Available</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.card -->
            {{-- <div class="col-lg-1"></div> --}}
            <!-- Calendar -->
            <div class="col-md-6">
                <div class="card bg-gradient-success" style="height: 460px">
                    <div class="card-header border-0">

                        <h3 class="card-title">
                            <i class="far fa-calendar-alt"></i>
                            Calendar
                        </h3>
                        <!-- tools card -->
                        <div class="card-tools">
                            <!-- button with a dropdown -->
                            <div class="btn-group">
                            </div>
                            <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pt-0">
                        <!--The calendar -->
                        <div id="calendar" style="width: 100%"></div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.card -->
            </div>
        </section>
        <!-- right col -->
    </div>
    <!-- /.row (main row) -->
</div><!-- /.container-fluid -->
@section('page-scripts')
<!-- ChartJS -->
<script src="{{url('')}}/assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="{{url('')}}/assets/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="{{url('')}}/assets/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="{{url('')}}/assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{url('')}}/assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{url('')}}/assets/dist/js/pages/dashboard.js"></script>

@endsection

@endsection