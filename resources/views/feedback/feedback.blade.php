@extends('layouts.master')

@section('title', 'Feedback ')

@section('top-styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@endsection

@section('content')
@section('breadcrumb','Feedback ')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fas fa-chair"> </i> Feedback
                         
                        </div>
                        <div class="panel-body">
                            <table id="DataTable" class="table table-bordered table-hover table-responsive" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort text-center" width="5%">S.No</th>
                                        <th>Customer Name</th>
                                        <th>Customer Email</th>
                                        <th>Customer Phone</th>
                                        <th>Created At</th>
                                        <th>Table Number</th>
                                        <th>Read / Unread</th>
                                        <th class="no-sort text-center" width="12%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
<!-- ./wrapper -->
@section('page-scripts')
<!-- DataTables -->
<script src="{{url('')}}/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#DataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route("feedback.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "customer_id",
                    "defaultContent": ""
                },
                {
                    "data": "customer_id",
                    "defaultContent": ""
                },
                {
                    "data": "customer_id",
                    "defaultContent": ""
                },
                {
                    "data": "created_at",
                    "defaultContent": ""
                },
                {
                    "data": "table_number",
                    "defaultContent": ""
                },
                {
                    "data": "is_readed",
                    "defaultContent": ""
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": 1,
                    "render": function (data, type, row, meta) {
                        return row.customer.full_name;
                    },
                },
                {
                    "targets": 2,
                    "render": function (data, type, row, meta) {
                        return row.customer.email;
                    },
                },
                {
                    "targets": 3,
                    "render": function (data, type, row, meta) {
                        return row.customer.phone;
                    },
                },
                {
                    "targets": -2,
                    "render": function (data, type, row, meta) {
                        // console.log(data);
                        if(data == 1)
                        {
                            return `<span class='text-success'>Read</span>`;
                        }else{
                            return `<span class='text-danger'>Unread</span>`;
                        }
                        
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("feedback.view",[":id"])}}';
                        edit = edit.replace(':id', data);
                        var checked = row.status == 1 ? 'checked' : null;
                        return `
                        <a href="` + edit + `" class="text-info p-1">
                           <button class="btn btn-success btn-sm"> <i class="fa fa-edit"></i> View</button>
                        </a>
                        `;
                    },
                },
            ],
            });

        });

</script>
@endsection
@endsection
