@extends('layouts.master')

@section('title', 'Feedback')

@section('top-styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
 <style>
    .box.box-primary {
        padding: 10px;
        border-top: 3px solid #00bcd4;
    }
    h3.box-title {
      font-size: 18px;
      color: #635d5d;
      font-weight: bold;
    }
   
    th {
        background: #f4f4f4 !important;
        color: #6f6f6f !important;
    }
 </style>
 @endsection

@section('content')
    @section('breadcrumb','Feedback')
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- jquery validation -->
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fas fa-chair"> </i> Feedback
                              <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button>
                        </div>
                        <div class="panel-body">
                            <div class="box box-primary">
                              <div class="box-header">
                                  <h3 class="box-title">Customer Details</h3>
                              </div>
                              <div class="box-body">
                                  <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                        <th>Customer Name</th>
                                        <th>Customer Email</th>
                                        <th>Customer Phone</th>
                                        <th>Created At</th>
                                        <th>Table Number</th>
                                      </tr>
                                    </thead>
                                      <tbody>
                                        <tr>
                                          <td>{{$feedback->customer->full_name}}</td>
                                          <td>{{$feedback->customer->email}}</td>
                                          <td>{{$feedback->customer->phone}}</td>
                                          <td>{{$feedback->customer->created_at}}</td>
                                          <td>{{$feedback->table_number}}</td>
                                      </tr>
                                  </tbody></table>                    
                              </div>
                            </div>
                            <hr>
                            <div class="box box-primary">
                              <div class="box-header">
                                  <h3 class="box-title">Comments</h3>
                              </div>
                              <div class="box-body">
                                  <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                        <th>Your Feedback will be help us to improve our services.	</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                          <tr>
                                            <td>{{$feedback->comments}}</td>
                                        </tr>
                                    </tbody>
                                  </table>                    
                              </div>
                            </div>
                            <hr>
                            <div class="box box-primary">
                              <div class="box-header">
                                  <h3 class="box-title">Customer Feedback</h3>
                              </div>
                              <div class="box-body">
                                  <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                        <th>S.No</th>
                                        <th>Question</th>
                                        <th>Option Selected</th>
                                        <th>Icon</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @php
                                          $i = 1;
                                      @endphp
                                        @foreach ($feedback->feedbackDetails as $option)
                                          <tr>
                                              <td>{{$i++}}</td>
                                              <td>{{$option->question->question}}</td>
                                              <td>{{$option->option->option}}</td>
                                              <td><img src="{{url('').'/icons/'.$option->option->icon}}" height="50px"></td>
                                          </tr>
                                        @endforeach
                                    </tbody>
                                  </table>                    
                              </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">

            </div>
            <!--/.col (right) -->
          </div>

    </div>
  <!-- ./wrapper -->
@section('page-scripts')
<!-- jquery-validation -->
<script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/assets/plugins/jquery-validation/additional-methods.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
      $('#quickForm').validate({
        rules: {
          name: { required: true, },
          description: { required: true, },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });

      function readURL(input, number) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview' + number).hide();
                $('#imagePreview' + number).fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
          }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });
    });
    </script>
@endsection
@endsection
