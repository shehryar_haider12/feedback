@extends('layouts.master')

@section('title', 'Add Option')

@section('top-styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
 @endsection

@section('content')
    @section('breadcrumb','Add Option')
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- jquery validation -->
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fas fa-chair"> </i> Add Option
                              <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button>
                        </div>
                        <div class="panel-body">
                          <!-- form start -->
                          <form action="{{$isEdit ? route('option.update',$option->id) : route('option.store')}}" role="form" method="POST" id="quickForm" enctype="multipart/form-data">
                            @csrf
                            @if ($isEdit)
                              <input type="hidden" name="_method" value="put">
                            @endif
                            {{-- <input type="hidden" name="user_type" value="1"> --}}
                            <div class="row">
                              <div class="col-md-4">
                                <div class="avatar-upload" style="height: 200px;">
                                    <div class="avatar-edit">
                                        <input type='file' name="icon" id="imageUpload1"
                                            accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload1"><span>{{ __('Featured Icon')}} </span></label>
                                    </div>
                                    <div class="avatar-preview">
                                        <div id="imagePreview1"
                                            style="background-image : url({{$isEdit ? url('').'/icons/'.$option->icon : url('').'/uploads/placeholder.jpg'}}">
                                        </div>
                                    </div>
                                </div>
                                <span class="text-danger">{{$errors->first('icon') ?? null}}</span>
                            </div>
                              <div class="col-md-8">
                                <div class="form-group col-md-10">
                                  <label for="option">Option</label>
                                  <input type="text" name="option" class="form-control" id="option" placeholder="Enter option" value="{{$option->option ?? old('option')}}">
                                  <span class="text-danger">{{$errors->first('option') ?? null}}</span>

                                </div>
                              {{-- </div>
                              <div class="col-md-6"> --}}
                                <div class="form-group col-md-10">
                                  <label for="sort_order">Sort Order</label>
                                  <input type="text" name="sort_order" class="form-control" id="sort_order" placeholder="Enter sort_order" value="{{$option->sort_order ?? old('sort_order')}}">
                                  <span class="text-danger">{{$errors->first('sort_order') ?? null}}</span>

                                </div>
                              </div>
                            </div>
                          {{-- <span class="text-danger">{{$errors->first('banner') ?? null}}</span> --}}
                              </div>
                              {{-- <div class="form-group col-2">
                                <label for="avail">Avail (per day)</label>
                                <input type="avail" name="avail" class="form-control" id="avail" placeholder="Enter Avail" value="{{$laboratory->avail ?? old('avail')}}">
                                <span class="text-danger">{{$errors->first('avail') ?? null}}</span>

                              </div>
                              <div class="form-group col-3">
                                <label for="avail">Total Days of Membership</label>
                                <input type="avail" name="days" class="form-control" id="days" placeholder="Enter days" value="{{$laboratory->days ?? old('days')}}">
                                <span class="text-danger">{{$errors->first('days') ?? null}}</span>

                              </div>
                              <div class="form-group col-12">
                                <label for="avail">Agreement Page Link</label>
                                <input type="avail" name="agreement_link" class="form-control" placeholder="Enter Page Link" value="{{$laboratory->agreement_link ?? old('agreement_link')}}">
                                <span class="text-danger">{{$errors->first('agreement_link') ?? null}}</span>

                              </div> --}}
                            </div>
                              <div class="card-footer">
                                  <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">

            </div>
            <!--/.col (right) -->
          </div>

    </div>
  <!-- ./wrapper -->
@section('page-scripts')
<!-- jquery-validation -->
<script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/assets/plugins/jquery-validation/additional-methods.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
      $('#quickForm').validate({
        rules: {
          name: { required: true, },
          description: { required: true, },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });

      function readURL(input, number) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview' + number).hide();
                $('#imagePreview' + number).fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
          }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });
    });
    </script>
@endsection
@endsection
