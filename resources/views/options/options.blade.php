@extends('layouts.master')

@section('title', 'Options ')

@section('top-styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@endsection

@section('content')
@section('breadcrumb','Options ')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fas fa-chair"> </i> Options
                            <a href="{{route('option.create')}}" class="col-md-2 float-sm-right">
                                <button type="button" class="btn btn-block btn-primary btn-md">Add Option </button>
                            </a>
                        </div>
                        <div class="panel-body">
                            <table id="DataTable" class="table table-bordered table-hover table-responsive" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort text-center" width="5%">S.No</th>
                                        <th>Icon</th>
                                        <th>Option</th>
                                        <th>Sort By</th>
                                        <th class="no-sort text-center" width="12%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
<!-- ./wrapper -->
@section('page-scripts')
<!-- DataTables -->
<script src="{{url('')}}/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#DataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route("option.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "icon",
                    "defaultContent": ""
                },
                {
                    "data": "option",
                    "defaultContent": ""
                },
                {
                    "data": "sort_order",
                    "defaultContent": ""
                },
                {
                    "data": "status",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": 1,
                    "render": function (data, type, row, meta) {
                        
                        return `<img src='{{url('')}}/icons/` + data +  `' height='50px' alt='image'/>`;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("option.edit",[":id"])}}';
                        edit = edit.replace(':id', row.id);
                        var checked = row.status == 1 ? 'checked' : null;
                        return `
                        <a href="` + edit + `" class="text-info p-1">
                            <i class="fa fa-edit"></i>
                        </a>
                        <a href="javascript:;" class="delete text-danger p-2" data-id="` + row.id + `">
                            <i class="fa fa-trash"></i>
                        </a>
                        `;
                    },
                },
            ],
            });

                $('.delete').click(function () {
                var deleteId = $(this).data('id');
                var $this = $(this);

                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#4fa7f3',
                    cancelButtonColor: '#d57171',
                    confirmButtonText: 'Yes, delete it!'
                }).then(function (result) {
                    if (result) {
                    axios
                    .post('{{route("option.delete")}}', {
                        _method: 'delete',
                        _token: '{{csrf_token()}}',
                        id: deleteId,
                    })
                    .then(function (response) {
                        console.log(response);

                        swal(
                        'Deleted!',
                        'Facility has been deleted.',
                        'success'
                        )

                        table
                        .row($this.parents('tr'))
                        .remove()
                        .draw();
                    })
                    .catch(function (error) {
                        console.log(error);
                        swal(
                        'Failed!',
                        error.response.data.error,
                        'error'
                        )
                    });
                    }
                })
                });
    });

</script>
@endsection
@endsection
