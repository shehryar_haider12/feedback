@extends('layouts.master_web')
@section('title','Add Details')
@section('custom-style')
@section('body-id','add_your_details')

<style>
       .input_hidden {
            position: absolute;
            left: -9999px;
        }

        .selected {
            background-color: #c42c2f;
            border-radius: 30px;
            padding: 1px 15px 1px 2px;
        }

        #emotion label {
            display: inline-block;
            cursor: pointer;
        }

        #emotion label img {
            padding: 3px;
        }
        label {
            color: white;
            font-size: 23px;
            font-weight: bold;
        }
   </style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
<div class="col-md-12 shadow">
    <nav class="navbar navbar-expand-md navbar-dark ">
        <a class="navbar-brand" href="#">
            <img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="220px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    </div>
</div>
</div>
<section id="add_details">
    <div class="container">
        <div class="row ">
            <div class="col-md-10 offset-md-2  ">
                <h2>Add Your Details</h2>
                
                <form class="pad_top" action="{{route('thank_you')}}" method="POST">
                    @csrf
                  <div class="row ">
                    <div class="col-md-4">
                      <div class="form-group">
                        <input type="name" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Full Name">
                    </div>
                  </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <input type="text" name='phone' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Phone">
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email">
                            </div>
                        </div>
                    </div>

                    <div class="row ">
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="comments" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" cols="30" rows="10" placeholder="Your Comments"></textarea>
                            </div>


                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn_sub">Submit</button>
                </form>
            </div>
        </div>
    </div>
</section>

@section('custom-script')
    <script>
        $('#emotion input:radio').addClass('input_hidden');
        $('#emotion label').click(function(){
            $(this).addClass('selected').siblings().removeClass('selected');
            $(this).prop('checked', false);
        });
    </script>
@endsection
@endsection