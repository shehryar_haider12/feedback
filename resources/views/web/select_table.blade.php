@extends('layouts.master_web')
@section('title','Select Table')

@section('content')

@section('body-id','select_table_page')
<style>
    .selectin::after {
        content: '\25BC';
        position: absolute;
        top: 0;
        right: 0;
        padding: 0 1em;
        background: #34495e;
        cursor: pointer;
        pointer-events: none;
        -webkit-transition: .25s all ease;
        -o-transition: .25s all ease;
        transition: .25s all ease;
    }

    /* Transition */
    .selectin:hover::after {
        color: #f39c12;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 shadow">
            <nav class="navbar navbar-expand-md navbar-dark ">
                <a class="navbar-brand" href="#">
                    <img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="220px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                    aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
        </div>
    </div>
</div>
<section id="select_tbl_sce">
    <div class="container">
        <center>
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <p class="hed"><img src="{{url('')}}/web_assets/images/Group 48.png"></p>

                    <p class="hed">Select table</p>
                    <form action="{{route('food_taste')}}">

                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <select class="selectin" name="table">
                                    <option value="" disabled>SELECT TABLE</option>
                                    @foreach ($tables as $table)
                                    <option value="{{$table->id}}">{{$table->table_no}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 offset-md-3">
                                <a href="{{route('food_taste')}}"><button type="submit"
                                        class="btn btn-primary btn_sub ">Next</button></a>
                            </div>
                        </div>

                    </form>


                </div>
            </div>
        </center>
    </div>
</section>
@endsection