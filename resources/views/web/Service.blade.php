@extends('layouts.master_web')
@section('title','Service')
@section('custom-style')
@section('body-id','service_page')

<style>
       .input_hidden {
            position: absolute;
            left: -9999px;
        }

        .selected {
            background-color: #c42c2f;
            border-radius: 30px;
            padding: 1px 15px 1px 2px;
        }

        #emotion label {
            display: inline-block;
            cursor: pointer;
        }

        #emotion label img {
            padding: 3px;
        }
        label {
            color: white;
            font-size: 23px;
            font-weight: bold;
        }
   </style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
<div class="col-md-12 shadow">
    <nav class="navbar navbar-expand-md navbar-dark ">
        <a class="navbar-brand" href="#">
            <img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="220px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    </div>
</div>
</div>
<section id="Service_feed_back_form">
    <div class="container">
        <center>
        <div class="row offset-md-1">
            <div class="col-md-12">
                <h2>{{$question->question}}</h2>
                <br>
                <br>
                <br>
                <form action="{{route('details')}}" method="get">
                    <input type="hidden" name="table" value="{{$_GET['table']}}">
                    <input type="hidden" name="question" value="{{$question->id}}">
                    <div class="row pd_top">
                        <div id="emotion" class="emotion">
                            @php
                                $options = options();
                                // $i=
                            @endphp
                            <div class="row offset-md-1">
                                @foreach ($options as $option)
                                    <div class="col-md-2 col-sm-4 col-xs-6 mob">
                                        <label id="emoji{{$option->id}}" for="sad{{$option->id}}">
                                            <img src="{{url('')}}/icons/{{$option->icon}}" alt="{{$option->option}}" height="80px"/>
                                            <span class="option_name">{{$option->option}}</span>
                                            <input type="radio" value="{{$option->id}}" name="option" id="{{$option->id}}" />
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="row col-md-12">
                            {{-- <div class="col-md-6"></div> --}}
                        <div class="col-md-12">
                        <button type="submit" style="margin-top: 100px; margin-right: 90px;" class="btn btn-secondary btn_next" data-dismiss="modal">Next</button>
                       </div>
                    </div>
                    </div>
            </form>
            </div>
    </div>
        </center>
    </div>
</section>
@section('custom-script')
    <script>
        $('.emotion input:radio').addClass('input_hidden');
        $('.emotion label').click(function(){
            var id = $(this).attr('id');
            console.log('#'+id+' input');
            $('.emotion label span').removeClass('select');
            $('.emotion label img').removeClass('select_img');
            $('.emotion label input[type=radio]').removeAttr('checked');
            $('#'+id+' input[type=radio]').attr('checked', 'checked');
            $('#'+id+' span').addClass('select');
            $('#'+id+' img').addClass('select_img');
        });
    </script>
@endsection

@endsection