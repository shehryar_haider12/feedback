@extends('layouts.master_web')
@section('title','Service')
@section('body-id','thank_u')

@section('content')
<div class="container-fluid">
    <div class="row">
<div class="col-md-12 shadow">
    <nav class="navbar navbar-expand-md navbar-dark ">
        <a class="navbar-brand" href="#">
            <img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="220px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    </div>
</div>
</div>
<section id="thanks">
    <div class="container">
        <div class="row">
        <div class="col-md-12">
            <h3 class="text-center">Thank you <br>For your Feedback!</h3>

            <div class="row">
                <div class="col-md-4 offset-md-4">
                    <img src="{{url('')}}/web_assets/images/Group 559.png" width="100%" height="300px" class="below_img">
                    <p class="go_btn"><a href="{{route('select_table')}}"><i class="fa fa-chevron-left"></i>&nbspGo Back</a></p>
            </div>

            </div>
        </div>

    </div>
    </div>
</section>

@endsection