@extends('layouts.master_web')
@section('title','Login')

@section('body-id','Login_page')
@section('content')

<div class="container-fluid">
    <div class="row">
<div class="col-md-12 shadow">
    <nav class="navbar navbar-expand-md navbar-dark ">
        <a class="navbar-brand" href="#">
            <img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="220px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    </div>
</div>
</div>
<section id="login_sce">
    <div class="container">
        <center>
        <div class="row">
          <div class="col-md-8 offset-md-2">
              <p class="hed"><img src="{{url('')}}/web_assets/images/Group 336.png"></p>

              <p class="hed">Login</p>
             <form action="{{route('login_waiter')}}" method="POST">
                @csrf
                 <div class="row">
                  <div class="col-md-6 offset-md-3">
                      <div class="form-group">
                          <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Email">
                    <span></span>
                      </div>
                      <span class="text-danger">{{$errors->first('email') ?? null}}</span>

                  </div>
                 </div>
                  <div class="row">
                     <div class="col-md-6 offset-md-3">
                         <div class="form-group">
                             <input type="password" class="form-control" name="password" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Password">
                         </div>
                         <span class="text-danger">{{$errors->first('password') ?? null}}</span>
                     </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6 offset-md-3">
                  <button type="submit" class="btn btn-primary btn_sub ">Login</button>
                      </div>
                  </div>

              </form>
              
            </div>
        </div>
    </center>
    </div>
</section>
@endsection