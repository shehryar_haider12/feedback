<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="A Content Management System designed by abc.com">
  <meta name="author" content="abc.com">

  <link rel="shortcut icon" href="{{url('')}}/assets/custom/images/favicon3.ico">

  <title>Login - Rehmat-e-Sheeren</title>

  <link href="{{url('')}}/assets/custom/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/style.css" rel="stylesheet" type="text/css" />

  <link href="{{url('')}}/assets/custom/css/animate.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/custom.css" rel="stylesheet" type="text/css" />

  <script src="{{url('')}}/assets/custom/js/modernizr.min.js"></script>
  <style>
  </style>
</head>

<body>

  <div class="account-pages bg-dark-theme">
    <div id="canvas-wrapper">
      <canvas id="demo-canvas"></canvas>
    </div>
  </div>
  <div class="clearfix"></div>

  <div class="login_page_custom">
    <div class="wrapper-page login_custom_con">
      <div class="animated fadeInDown">
        <div class="text-center bg-white b_r_5 bb_l_r_0 p10">
            <h3>Rehmat-e-Sheeren</h3>  
        </div>
        <div class="card-box bg-black-transparent4 bt_l_r_0">
          <div class="panel-heading text-center">

            <h1 class="text-white m0">
              <strong> Reset Password </strong>
            </h1>
          </div>


          <div class="p-20">
            <form class="form-horizontal" action="{{ route('password.request') }}" method="post" autocomplete="off">
              @csrf
              <input type="hidden" name="token" value="{{ $token }}">
              @if ($errors->any())
              <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                  ×
                </button>
                @foreach ($errors->all() as $item)
                {{ $item }}
                <br>
                @endforeach
              </div>
              @endif

              <div class="form-group ">
                <div class="col-12">
                  <input class="form-control" type="email" name="email" required='' placeholder="Email" value="{{ $email ?? old('email') }}">
                </div>
              </div>
              <div class="form-group ">
                <div class="col-12">
                  <input class="form-control" type="password" name="password" required='' placeholder="New Password">
                </div>
              </div>

              <div class="form-group">
                <div class="col-12">
                  <input class="form-control" type="password" required='' name="password_confirmation" placeholder="Confirm">
                </div>
              </div>

              <div class="form-group text-center m-t-40">
                <div class="col-12">
                  <button class="btn btn-light-theme btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                </div>
              </div>

            </form>

          </div>
        </div>
      </div>
    </div>
  </div>




  <script>
    var resizefunc = [];
  </script>

  <!-- jQuery  -->
  <script src="{{url('')}}/assets/custom/js/jquery.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/popper.min.js"></script>
  <!-- Popper for Bootstrap -->
  <script src="{{url('')}}/assets/custom/js/bootstrap.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/detect.js"></script>
  <script src="{{url('')}}/assets/custom/js/fastclick.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.slimscroll.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.blockUI.js"></script>
  <script src="{{url('')}}/assets/custom/js/waves.js"></script>
  <script src="{{url('')}}/assets/custom/js/wow.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.nicescroll.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.scrollTo.min.js"></script>

  <!-- lOGIN Page Plugins -->
  <script src="{{url('')}}/assets/custom/pages/login/EasePack.min.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/rAF.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/TweenLite.min.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/login.js"></script>


  <script src="{{url('')}}/assets/custom/js/jquery.core.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.app.js"></script>

  <script type="text/javascript">
    jQuery(document).ready(function () {

      // Init CanvasBG and pass target starting location
      CanvasBG.init({
        Loc: {
          x: window.innerWidth / 2,
          y: window.innerHeight / 3.3
        },
      });

    });
  </script>

</body>

</html>
