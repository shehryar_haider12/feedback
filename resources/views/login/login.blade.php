<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="A Content Management System designed by abc.com">
  <meta name="author" content="abc.com">

  {{-- <link rel="shortcut icon" href="{{url('')}}/assets/images/favicon3.ico"> --}}

  <title>Login - Feedback App</title>
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/fontawesome-free/css/all.min.css">

  <link href="{{url('')}}/assets/custom/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/icons.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/style.css" rel="stylesheet" type="text/css" />

  <link href="{{url('')}}/assets/custom/css/animate.min.css" rel="stylesheet" type="text/css" />
  <link href="{{url('')}}/assets/custom/css/custom.css" rel="stylesheet" type="text/css" />

  <script src="{{url('')}}/assets/custom/js/modernizr.min.js"></script>
  {{-- <script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js"></script>
  <link rel="manifest" href="{{url('')}}/manifest.json">
  <link rel="manifest" href="{{url('')}}/firebase-messaging-sw.js">
  <script src="https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js"></script> --}}

</head>

<body>

  <div class="account-pages bg-dark-theme">
    <div id="canvas-wrapper">
      <canvas id="demo-canvas"></canvas>
    </div>
  </div>

  <div class="login_page_custom">
    <div class="wrapper-page login_custom_con">
      <div class="animated fadeInDown">
        <div class="text-center bg-white b_r_5 bb_l_r_0 p10">
          {{-- <img src="{{url('/uploads/images').'/'}}" alt="Site logo" width="50%" /> --}}
          <h2>Feedback App</h2>
        </div>
        <div class="card-box bg-black-transparent4 bt_l_r_0">
          <div class="panel-heading text-center">

            <h1 class="text-white m0">
              <strong>Admin Login </strong>
            </h1>
          </div>

          <div class="p-20">
            <form class="form-horizontal" action="{{route('login')}}" method="POST" id="loginForm">
              <div>
                @csrf
                <input type="hidden" name="device_token" id="device_token">
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    ×
                  </button>
                  @foreach ($errors->all() as $item)
                  {{ $item }}
                  <br>
                  @endforeach
                </div>
                @endif
                <div class="form-group ">
                  <div class="col-12">
                    <input class="form-control" type="email" name="email" required='' placeholder="Email">

                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-12">
                  <input class="form-control" type="password" required='' name="password" placeholder="Password">
                </div>
              </div>

              <div class="form-group ">
                <div class="col-12">
                  <div class="checkbox checkbox-theme-light">
                    <input id="checkbox-signup" type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}>
                    <label for="checkbox-signup">
                      Remember me
                    </label>
                  </div>

                </div>
              </div>

              <div class="form-group text-center m-t-40">
                <div class="col-12">
                  <button class="btn btn-light-theme btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                </div>
              </div>
              <div class="form-group m-t-30 m-b-0">
                <div class="row">
                <div class="col-7">
                  <a href="{{ route('password.request')}}" class="text-light-theme-link" style="color: white">
                    <i class="fas fa-lock"></i> Forgot your password?</a>
                </div>
                <div class="col-5">
                  <a href="{{ route('register')}}" class="text-light-theme-link" style="color: white">
                    <i class="fas fa-user"></i> create an account</a>
                </div>
              </div>
              </div>
            </form>
          </div>


        </div>
      </div>
    </div>
  </div>

  <script>
    var resizefunc = [];
  </script>

  <!-- jQuery  -->
  <script src="{{url('')}}/assets/custom/js/jquery.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/popper.min.js"></script>
  <!-- Popper for Bootstrap -->
  <script src="{{url('')}}/assets/custom/js/bootstrap.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/detect.js"></script>
  <script src="{{url('')}}/assets/custom/js/fastclick.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.slimscroll.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.blockUI.js"></script>
  <script src="{{url('')}}/assets/custom/js/waves.js"></script>
  <script src="{{url('')}}/assets/custom/js/wow.min.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.nicescroll.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.scrollTo.min.js"></script>

  <!-- lOGIN Page Plugins -->
  <script src="{{url('')}}/assets/custom/pages/login/EasePack.min.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/rAF.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/TweenLite.min.js"></script>
  <script src="{{url('')}}/assets/custom/pages/login/login.js"></script>

  <script src="{{url('')}}/assets/custom/js/jquery.core.js"></script>
  <script src="{{url('')}}/assets/custom/js/jquery.app.js"></script>
  {{-- <script src="{{url('')}}/js/firebase.js"></script> --}}

  <script type="text/javascript">
    jQuery(document).ready(function () {

      // Init CanvasBG and pass target starting location
      CanvasBG.init({
        Loc: {
          x: window.innerWidth / 2,
          y: window.innerHeight / 3.3
        },
      });

      fetch('https://ipapi.co/json/')
      .then(response => response.json())
      .then((response) => {
        $('#loginForm').append(`<input type="hidden" name="last_login_details" value='`+JSON.stringify(response)+`'>`);
      })
    });
  </script>

</body>

</html>
