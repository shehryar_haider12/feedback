<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{url('')}}/web_assets/vendors/jquery-ui/jquery-ui.min.css" media="all">
    <link rel="stylesheet" href="{{url('')}}/web_assets/vendors/bootstrap/css/bootstrap.min.css" media="all">
    <link rel="stylesheet" href="{{url('')}}/web_assets/vendors/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="{{url('')}}/web_assets/css/style.css">
    <title>@yield('title')</title>
    @yield('custom-style')
</head>

<body id="@yield('body-id')" class="col-md-12">
    
    @yield('content')

    <script src="{{url('')}}/web_assets/vendors/jquery.min.js"></script>
    <script src="{{url('')}}/web_assets/vendors/popper.min.js"></script>
    <script src="{{url('')}}/web_assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{url('')}}/web_assets/vendors/fontawesome/js/all.min.js"></script>
    <script src="{{url('')}}/web_assets/vendors/OwlCarousel/dist/owl.carousel.min.js"></script>
    <script src="{{url('')}}/web_assets/js/script.js"></script>
    <script>
        $(document).on("scroll", function(){
            if
            ($(document).scrollTop() > 86){
                $("#banner").addClass("shrink");
            }
            else
            {
                $("#banner").removeClass("shrink");
            }
        });
    </script>
    @yield('custom-script')
</body>

</html>