<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Feedback App - @yield('title')</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{csrf_token()}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  {{-- <link rel="stylesheet" href="{{url('')}}/assets/dist/css/ionicons.min.css"> --}}

  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('')}}/assets/dist/css/adminlte.min.css">
  {{-- Image-Upload --}}
  <link rel="stylesheet" href="{{url('')}}/assets/custom/css/image-upload.css">
  {{-- SweetAlert2 --}}
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/sweet-alert2/sweetalert2.min.css">
  {{-- Switchery Css --}}
  <link href="{{url('')}}/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />

  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="{{url('')}}/assets/plugins/summernote/summernote-bs4.css">
    {{-- FireBase Integeration --}}
    {{-- <script src="https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js"></script> --}}
   <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}

  @yield('top-styles')
  <style>
    .panel-heading {
        height: 60px;
        padding: 13px;
        background: #007bff;
        color: white;
        font-size: 20px;
   }
   .panel-body {
       padding: 20px;
       border: 1px solid #80808047;
   }
   th {
        background: #017bffdb;
        color: white;
    }
    button.btn.btn-block.btn-primary.btn-md {
        background: #343a4096;
    }
    form#advanceSearch {
        background: #7cabff;
        padding: 20px;
        margin-bottom: 25px;
        border: 1px solid #dbdbdb;
    }
    button#search {
       background: #017bff;
    }
    button.dt-button {
        padding: 5px 12px;
        margin: 0px 7px;
        float: left;
        border: 1px solid #80808085;
        border-radius: 5px;
        color: #614d4d;
    }
</style>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        @include('layouts.header')
        @include('layouts.sidebar')
          <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">@yield('breadcrumb')</li>
                    </ol>
                    </div>
                </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                @yield('content')
            </section>
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2020-2021 <a href="https://zerk.com.au">Feedback</a>.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.0.5
            </div>
        </footer>
            
        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->

    </div>
<!-- jQuery -->
<script src="{{url('')}}/assets/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{url('')}}/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
{{-- Nice Scroll --}}
<script src="{{url('')}}/assets/custom/js/jquery.nicescroll.js"></script>
{{-- SweetAlert2 --}}
<script src="{{url('')}}/assets/plugins/sweet-alert2/sweetalert2.min.js"></script>
{{-- Axios --}}
<script src="{{url('')}}/assets/custom/js/axios.min.js"></script>
{{-- Switchery --}}
<script src="{{url('')}}/assets/plugins/switchery/js/switchery.min.js"></script>

<!-- Bootstrap 4 -->
<script src="{{url('')}}/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- daterangepicker -->
<script src="{{url('')}}/assets/plugins/moment/moment.min.js"></script>
<script src="{{url('')}}/assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{url('')}}/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="{{url('')}}/assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{url('')}}/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="{{url('')}}/assets/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('')}}/assets/dist/js/demo.js"></script>
<script>
$(document).ready(function(){
    Notification.requestPermission().then(function(result) {
            // console.log("adsfsdfa");
    });


    $('#read-notification').click(function () {
        var $this = $(this);
        var id = $this.data('user');
        // console.log('adfadfsf');
        axios
            .post('{{route("dashboard")}}', {
            _token: '{{csrf_token()}}',
            _method: 'patch',
            id: id,
            })
            .then(function (responsive) {
                $('.number-alert').empty().html(0);
                // $('.data-notify').empty();
                console.log(responsive);
            })
            .catch(function (error) {
            console.log(error);
            });
    });
});
</script>
@yield('page-scripts')
@yield('custom-script')
</body>
</html>
