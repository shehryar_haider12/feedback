<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('dashboard')}}" class="brand-link">
        <img src="{{url('')}}/assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Feedback App</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{url('')}}/uploads/{{Auth::user()->avatar ?? 'dummy-user.jpeg'}}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{Auth::user()->full_name ?? null}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <li class="nav-item">
                    <a href="{{route('dashboard')}}" class="nav-link {{Route::currentRouteName() == 'dashboard' ? 'active' : null}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('users.settings')}}" class="nav-link {{Route::currentRouteName() == 'users.settings' ? 'active' : null}}">
                        <i class="nav-icon fas fa-cogs mr-2"></i>
                        <p>Update Profile</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'feedback' || Route::currentRouteName() == 'feedback.view' ? 'active' : null}}">
                        <i class="nav-icon fa fa-desktop mr-2"></i>
                        <p>
                            Customer Feedback
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('feedback')}}" class="nav-link {{Route::currentRouteName() == 'feedback' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Feedback</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'table' || Route::currentRouteName() == 'table.create' ? 'active' : null}}">
                        <i class="nav-icon fas fa-chair mr-2"></i>
                        <p>
                            Tables
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('table')}}" class="nav-link {{Route::currentRouteName() == 'table' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Table</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('table.create')}}" class="nav-link {{Route::currentRouteName() == 'table.create' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Table</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'question' || Route::currentRouteName() == 'question.create' || Route::currentRouteName() == 'question.edit' ? 'active' : null}}">
                        <i class="nav-icon fas fa-question mr-2"></i>
                        <p>
                            Questions
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('question')}}" class="nav-link {{Route::currentRouteName() == 'question' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Question</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('question.create')}}" class="nav-link {{Route::currentRouteName() == 'question.create' || Route::currentRouteName() == 'question.edit' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Question</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link {{Route::currentRouteName() == 'option' || Route::currentRouteName() == 'option.create' || Route::currentRouteName() == 'option.edit' ? 'active' : null}}">
                        <i class="nav-icon fas fa-check mr-2"></i>
                        <p>
                            Options
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('option')}}" class="nav-link {{Route::currentRouteName() == 'option' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>View Option</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('option.create')}}" class="nav-link {{Route::currentRouteName() == 'option.create' || Route::currentRouteName() == 'option.edit' ? 'active' : null}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Option</p>
                            </a>
                        </li>
                    </ul>
                </li>
               
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
