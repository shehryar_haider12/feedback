<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','SiteController@login');
Route::post('/login-waiter','SiteController@loginWaiter')->name('login_waiter');
Route::get('/select-table','SiteController@selectTable')->name('select_table');
Route::get('/food-taste','SiteController@foodTaste')->name('food_taste');
Route::get('/food-quality','SiteController@foodQuality')->name('food_quality');
Route::get('/ambiance','SiteController@ambiance')->name('ambiance');
Route::get('/service','SiteController@service')->name('service');
Route::get('/details','SiteController@details')->name('details');
Route::post('/thank-you','SiteController@thankyou')->name('thank_you');


Route::group(['prefix' => 'admin'], function () {

// Route::Dashboard
Route::group(['prefix' => 'dashboard'], function () {
    
    Route::get('/', 'HomeController@dashboard')->name('dashboard');
    Route::get('/settings', 'HomeController@settings')->name('users.settings');
    Route::patch('/settings', 'HomeController@patch')->name('users.settings');
});

// Route::Tables
Route::group(['prefix' => 'tables'], function () {

    Route::get('','TableController@index')->name('table');
    Route::get('/datatable','TableController@datatable')->name('table.datatable');
    Route::get('/create','TableController@create')->name('table.create');
    Route::post('/store','TableController@store')->name('table.store');
    Route::get('/edit/{table}','TableController@edit')->name('table.edit');
    Route::put('/update/{table}','TableController@update')->name('table.update');
    Route::patch('/status','TableController@status')->name('table.status');
    Route::delete('/delete','TableController@destroy')->name('table.delete');
});

// Route::Questions
Route::group(['prefix' => 'questions'], function () {

    Route::get('','QuestionController@index')->name('question');
    Route::get('/datatable','QuestionController@datatable')->name('question.datatable');
    Route::get('/create','QuestionController@create')->name('question.create');
    Route::post('/store','QuestionController@store')->name('question.store');
    Route::get('/edit/{question}','QuestionController@edit')->name('question.edit');
    Route::put('/update/{question}','QuestionController@update')->name('question.update');
    Route::patch('/status','QuestionController@status')->name('question.status');
    Route::delete('/delete','QuestionController@destroy')->name('question.delete');
});

// Route::Options
Route::group(['prefix' => 'options'], function () {

    Route::get('','OptionController@index')->name('option');
    Route::get('/datatable','OptionController@datatable')->name('option.datatable');
    Route::get('/create','OptionController@create')->name('option.create');
    Route::post('/store','OptionController@store')->name('option.store');
    Route::get('/edit/{option}','OptionController@edit')->name('option.edit');
    Route::put('/update/{option}','OptionController@update')->name('option.update');
    Route::patch('/status','OptionController@status')->name('option.status');
    Route::delete('/delete','OptionController@destroy')->name('option.delete');
});

// Route::Feedback
Route::group(['prefix' => 'feedbacks'], function () {

    Route::get('','FeedBackController@index')->name('feedback');
    Route::get('/datatable','FeedBackController@datatable')->name('feedback.datatable');
    Route::get('/view/{feedback}','FeedBackController@view')->name('feedback.view');
});


Auth::routes();
});
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Cache Cleared Route
Route::get('exzed/cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
   return 'cache clear';
});


