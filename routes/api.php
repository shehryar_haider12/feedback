<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//---------------------------- User Routes

// Route::group(['prefix' => 'memberships'], function () {
//     Route::get('/all','Api\SiteController@getAll');
// });


// Route::any('{url?}/{sub_url?}', function () {
//     return result(false, 404, "Route not Found.");
// });

Route::post('/login', 'Api\LoginController@login');
Route::post('/feedback', 'Api\CustomerFeedbackController@feedback');
// Route::get('/all','Api\LoginController@users');
Route::get('/tables','Api\TableController@tables');
Route::get('/questions','Api\TableController@questions');
Route::get('/options','Api\TableController@options');
